# Weather App

Python app on Raspberry Pi Zero W that displays weather from OpenWeatherMap on small display.

Weather icons are Symbolicons by Sonny Sekhon. 

Interface icons are from [Erik Flowers](https://erikflowers.github.io/weather-icons/).

The background image was found online years ago and is also not my own.

This code was based on snippets from an Adafruit tutorial found [here](https://learn.adafruit.com/adafruit-2-8-and-3-2-color-tft-touchscreen-breakout-v2/python-wiring-and-setup).

## Example Output

Screen 1
<img src='./images/screen1.jpg' width=250>

Screen 2
<img src='./images/screen2.jpg' width=250>

Screen 3
<img src='./images/screen3.jpg' width=250>

Screen 4
<img src='./images/screen4.jpg' width=250>

Screen 5
<img src='./images/screen5.jpg' width=250>

