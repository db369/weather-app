#!/usr/bin/python3
import digitalio
import board
from PIL import Image, ImageDraw, ImageFont, ImageChops
import adafruit_rgb_display.ili9341 as ili9341
import time
import requests
import json
from datetime import datetime
import os
from icons import icon_conditions

# OWM_API_KEY set locally as enironment variable
api_key = os.environ.get("OWM_API_KEY")

# Configuration for CS and DC pins (these are PiTFT defaults):
cs_pin = digitalio.DigitalInOut(board.CE0)
dc_pin = digitalio.DigitalInOut(board.D25)
reset_pin = digitalio.DigitalInOut(board.D24)

# Config for display baudrate (default max is 24mhz):
BAUDRATE = 24000000

# Setup SPI bus using hardware SPI:
spi = board.SPI()

# pylint: disable=line-too-long
# Create the display:

disp = ili9341.ILI9341(
    spi,
    rotation=90,  # 2.2", 2.4", 2.8", 3.2" ILI9341
    cs=cs_pin,
    dc=dc_pin,
    rst=reset_pin,
    baudrate=BAUDRATE,
)
# pylint: enable=line-too-long

# Create blank image for drawing.
# Make sure to create image with mode 'RGB' for full color.
if disp.rotation % 180 == 90:
    height = disp.width  # we swap height/width to rotate it to landscape!
    width = disp.height
else:
    width = disp.width  # we swap height/width to rotate it to landscape!
    height = disp.height
#image = Image.new("RGB", (width, height))

icon_path = "/home/pi/weather_app/icons/"
bkgd = "/home/pi/weather_app/icons/bkgd_4.jpg"
image = Image.open(bkgd)

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)


url = 'http://api.openweathermap.org/data/2.5/weather?id=4560349&appid=' + str(api_key) + '&units=imperial'

f_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?id=4560349&appid=' + str(api_key) + '&units=imperial'

headers = {"User-Agent": 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.88 Safari/537.36'}

#def wipe(img):
#    for i in range(0,220):
#        img.crop((i,0,width-i,height))
#        image.paste(img)
#        disp.image(image)

def upd_weather(url, headers):

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None
    

def dejavu(x, f = 0):
    font_path = "/usr/share/fonts/truetype/dejavu/"

    font_names = {
        0:"DejaVuSans.ttf",
        1:"DejaVuSans-Bold.ttf",
        2:"DejaVuSerif.ttf",
        3:"DejaVuSerif-Bold.ttf"
    }
    
    return ImageFont.truetype(font_path + font_names[f], x)

font = dejavu(32, 2)
font2 = dejavu(48, 3)
sub_font = dejavu(28, 2)


def window1(image):
    # Draw a black filled box to clear the image.
    #draw.rectangle((0, 0, width, height), outline=0, fill=(0, 0, 0))
    disp.image(image)

    im = Image.open(icon_path + icon_conditions[icon][tod])
    im = im.resize((128, 128), Image.BICUBIC)
    image.paste(im,im)

    draw.text((130,40), str(round(temp,1)) + '°F', font=font2, fill=(250,250,250))
    draw.text((20,150), str.title(description), font=font if len(description)<15 else sub_font, fill=(255,255,250))
    txt = str(round(tmax)) + ' / ' + str(round(tmin)) + '°F'
    draw.text((140,100), txt, font=sub_font, fill=(255,255,250))
    draw.text((20,192), "Feels like " + str(round(flike,1)) + '°F', font=sub_font, fill=(255,255,250))
    return(image)

def window2(img): # humidity
    disp.image(img)
    
    im = Image.open(icon_path + "humidity.png").resize((160,160), Image.BICUBIC)
    img.paste(im,(-15,25),im)
    h_title = "Humidity"
    h_txt = str(humidity) + '%'

    draw.text((130,25), h_title, font = font, fill=(250,250,250))
    draw.text((140,80), h_txt, font = dejavu(64 if humidity < 100 else 48,3), fill=(250,250,250))

    return(img)

def window3(img): # bar
    #draw.rectangle((0, 0, width, height), outline=0, fill=(0, 0, 0))
    disp.image(img)
    
    imb = Image.open(icon_path + "barometer.png").resize((160,160), Image.BICUBIC) 
    img.paste(imb,(160,20),imb)

    p_title = "Barometric"
    p_title2 = "Pressure"
    p_txt = str(pressure) + ' hPa'

    draw.text((10,20), p_title, font = font, fill=(250,250,250))
    draw.text((10,55), p_title2, font = font, fill=(250,250,250))
    draw.text((10,150), p_txt, font = font2, fill=(250,250,250))

    return(img)

def window4(img): # wind
    #draw.rectangle((0, 0, width, height), outline=0, fill=(0, 0, 0))
    disp.image(img)
    
    if(wind_deg == -1):
        imw = Image.open(icon_path + "na.png").resize((128,128), Image.BICUBIC)
    else:
        imw = Image.open(icon_path + "wind-deg.png").resize((120,120), Image.BICUBIC).rotate(-wind_deg)
    
    img.paste(imw,(0,20),imw)

    w_title = "Wind Speed"
    w_txt = str(round(wind_speed,1))

    draw.text((120,20), w_title, font = font, fill=(250,250,250))
    draw.text((155,70), w_txt, font = font2, fill=(250,250,250))
    draw.text((170,120), "mph", font = font, fill=(250,250,250))

    if(gust):
        im2 = Image.open(icon_path + "wind-solid.png").resize((64,64), Image.BICUBIC)
        img.paste(im2,(15,160),im2)
        gtxt1 = "Gusts to" 
        gtxt2 = str(round(wind_gust,1)) + " mph"
        draw.text((95,160), gtxt1, font = sub_font, fill=(250,250,250))
        draw.text((95,195), gtxt2, font = sub_font, fill=(250,250,250))

    return(img)

def window5(img):
    #draw.rectangle((0, 0, width, height), outline=0, fill=(0, 0, 0))
    disp.image(img)
    
    im = Image.open(icon_path + "sunrise.png").resize((64,64), Image.BICUBIC)
    im2= Image.open(icon_path + "sunset.png").resize((64,64), Image.BICUBIC)
    img.paste(im,(15,20),im)
    img.paste(im2,(15,75),im2)
    sr_txt = str.lower(datetime.fromtimestamp(sunrise).strftime('%-I:%M%p'))
    ss_txt = str.lower(datetime.fromtimestamp(sunset).strftime('%-I:%M%p'))

    draw.text((90,28), sr_txt, font = font, fill=(250,250,250))
    draw.text((90,90), ss_txt, font = font, fill=(250,250,250))
    draw.text((10,160), 'Updated at ' + str.lower(datetime.fromtimestamp(update).strftime('%-I:%M%p')), font=sub_font, fill=(255,255,250))  
    return(img)

x = 0

while(True):
    if x == 0:
        y = upd_weather(url, headers)
        yf = upd_weather(f_url, headers)
        if y is not None:
            update = y["dt"]
            sunrise = y["sys"]["sunrise"]
            sunset = y["sys"]["sunset"]
            tz = y["timezone"]
            tod = 0 if (update >= sunrise) & (update < sunset) else 1
            icon = y["weather"][0]['id']
            cond = y["weather"][0]["main"]
            description = y["weather"][0]["description"]
            temp = y["main"]["temp"]
            flike = y["main"]["feels_like"]
            #tmax = y["main"]["temp_max"]
            #tmin = y["main"]["temp_min"]
            tmax = yf["list"][0]["temp"]["max"]
            tmin = yf["list"][0]["temp"]["min"]
            pressure = y["main"]["pressure"]
            humidity = y["main"]["humidity"]
            wind_speed = y["wind"]["speed"]
            wind_deg = y["wind"]["deg"] if ("deg" in y["wind"]) else -1
            if ("gust" in y["wind"]):
                wind_gust = y["wind"]["gust"]
                gust = True
            else: 
                gust = False

    elif x == 1:
        image = window1(image)

    elif x == 2:
        image = window2(image)

    elif x == 3:
        image = window3(image)

    elif x == 4:
        image = window4(image)

    else: 
        image = window5(image)

    disp.image(image)
    if x > 0:
        time.sleep(5)
    x = x + 1 if x < 5 else 0
    image = Image.open(bkgd)
    draw = ImageDraw.Draw(image) 

#wipe(image)
